(ns four-clojure.core
  (:gen-class))

(defn ex-22 [values]
  "Problem 22: Count a sequence."
  (loop [i 0 v values]
    (if (nil? v)
      i
      (recur (inc i) (next v)))))

(defn ex-23 [values]
  "Problem 23: Reverse a sequence."
  (loop [rev () vs values]
    (if (nil? vs)
      rev
      (recur (conj rev (first vs)) (next vs)))))

(defn ex-24 [values]
  "Problem 24: Sum it all up."
  (reduce + values))

(defn ex-25 [values]
  "Problem 25: Find the odd numbers."
  (filter odd? values))

(defn ex-26 [length]
  "Problem 26: Fibonacci Sequence."
  (take length
        ((fn fib [a b]
           (cons a (lazy-seq (fib b (+ a b)))))
         1 1)))

(defn ex-27 [values]
  "Problem 27: Palindrome Detector "
  (= (reverse (seq values)) (seq values)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
